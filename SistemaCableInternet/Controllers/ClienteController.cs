﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApplicationSistemaCableInternet.Interface;
using WebApplicationSistemaCableInternet.Models;

namespace WebApplicationSistemaCableInternet.Controllers
{
    public class ClienteController : Controller
    {
        private readonly IServicioCliente servicio;
        public ClienteController(IServicioCliente servicio)
        {
            this.servicio = servicio;
        }
        public IActionResult Index(string mensaje = "", bool ocurrioError = false)
        {
            if (!string.IsNullOrEmpty(mensaje))
            {
                this.ViewData["Clase"] = "success";
                this.ViewData["Mensaje"] = mensaje;
                
                if (ocurrioError)
                {
                    this.ViewData["Clase"] = "danger";
                }
            }

            List<Cliente> listaClientes = this.servicio.ConsultarClientes();
            return this.View(listaClientes);
        }

        [HttpGet]
        public IActionResult Crear()
        {
            return this.View();
        }

        [HttpPost]
        public IActionResult Crear(Cliente cliente)
        {
            string mensaje = "Ocurrio un error al intentar registrar el cliente, intente mas tarde.";
            bool ocurrioError = true;

            if (this.servicio.CrearCliente(cliente))
            {
                mensaje = "Cliente registrado exitosamente.";
                ocurrioError = false;
            }

            return RedirectToAction("Index", new { mensaje = mensaje, ocurrioError = ocurrioError});
        }

        public IActionResult Eliminar(int idCliente)
        {
            string mensaje = "Ocurrio un error al intentar eliminar el cliente, intente mas tarde.";
            bool ocurrioError = true;

            if (this.servicio.EliminarCliente(idCliente))
            {
                mensaje = "Cliente eliminado exitosamente.";
                ocurrioError = false;
            }

            return RedirectToAction("Index", new { mensaje = mensaje, ocurrioError = ocurrioError });
        }
    }
}
