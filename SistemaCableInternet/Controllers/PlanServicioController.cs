﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApplicationSistemaCableInternet.Interface;
using WebApplicationSistemaCableInternet.Models;

namespace WebApplicationSistemaCableInternet.Controllers
{
    public class PlanServicioController : Controller
    {
        private readonly IServicioPlanServicio servicio;
        public PlanServicioController(IServicioPlanServicio servicio)
        {
            this.servicio = servicio;
        }
        public IActionResult Index(string mensaje = "", bool ocurrioError = false)
        {
            if (!string.IsNullOrEmpty(mensaje))
            {
                this.ViewData["Clase"] = "success";
                this.ViewData["Mensaje"] = mensaje;

                if (ocurrioError)
                {
                    this.ViewData["Clase"] = "danger";
                }
            }


            List<PlanServicio> listaClientes = this.servicio.ConsultarListadoPlanServicio();
            return this.View(listaClientes);
        }

        public IActionResult Crear()
        {
            return this.View();
        }

        // GET: PlanServicioController/Edit/5
        public IActionResult Editar(int id)
        {
            var model = this.servicio.ConsultarPlanServicio(id);

            return View(model);
        }

        // POST: PlanServicioController/Edit/5
        [HttpPost]
        public IActionResult Editar(int id, PlanServicio planServicio)
        {
            try
            {
                string mensaje = "Ocurrio un error al intentar registrar el cliente, intente mas tarde.";
                bool ocurrioError = true;
                planServicio.PlanServicioId = id;

                if (this.servicio.ActualizarPlanServicio(planServicio))
                {
                    mensaje = "Registrado exitosamente.";
                    ocurrioError = false;
                }
                return RedirectToAction("Index", new { mensaje = mensaje, ocurrioError = ocurrioError });
            }
            catch
            {
                return View();
            }
        }

    }
}
