﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApplicationSistemaCableInternet.Interface;
using WebApplicationSistemaCableInternet.Models;

namespace WebApplicationSistemaCableInternet.Controllers
{
    public class FacturaController : Controller
    {
        private readonly IServicioFactura servicio;
        private readonly IServicioCliente servicioClientes;
        private readonly IServiceServicio servicioPlanes;
        public FacturaController(IServicioFactura servicio,
            IServicioCliente servicioClientes, IServiceServicio servicioPlanes)
        {
            this.servicio = servicio;
            this.servicioClientes = servicioClientes;
            this.servicioPlanes = servicioPlanes;
        }
        public IActionResult Index(string mensaje = "", bool ocurrioError = false)
        {
            if (!string.IsNullOrEmpty(mensaje))
            {
                this.ViewData["Clase"] = "success";
                this.ViewData["Mensaje"] = mensaje;

                if (ocurrioError)
                {
                    this.ViewData["Clase"] = "danger";
                }
            }

            List<Factura> lista = this.servicio.ConsultarListado();
            return this.View(lista);
        }


        public IActionResult Detalle(int facturaId)
        {
            var model = new Factura();
            // Obtener datos de la factura
            model = this.servicio.ConsultarFactura(facturaId);

            return this.View(model);
        }

        public IActionResult Crear(int clienteId)
        {
            var model = new Factura();
            // Obtener la información del cliente
            var cliente = this.servicioClientes.ConsultarCliente(clienteId);
            if (cliente == null)
            {
                return RedirectToAction("Index", new { mensaje = "Error al consultar la información", ocurrioError = true });
            }
            model.Cliente = cliente;

            // Obtener los servicios del cliente
            model.Servicios = this.servicioPlanes.ConsultarServiciosCliente(clienteId);
            foreach (var servicio in model.Servicios)
            {
                servicio.Pagos = this.servicio.ConsultarMesesPago(clienteId, servicio.ServicioId);
            }

            return this.View(model);
        }

        [HttpPost]
        public IActionResult Crear(Factura factura, int[] servicios, string[] detalles)
        {
            string mensaje = "Ocurrio un error al intentar guardar el registro, intente mas tarde.";
            bool ocurrioError = true;

            factura.Servicios = this.servicioPlanes.ConsultarServiciosCliente(factura.ClienteId);
            factura.DetallesFactura = new List<DetalleFactura>();
            var total = 0.0M;
            var descuento = 0.0M;

            // Calcular Total de la factura
            var totalPagosServicio = 0;
            foreach (var detalle in detalles)
            {
                var datos = detalle.Split('-');
                var servicio = factura.Servicios.Find(x => x.ServicioId.ToString() == datos[0]);
                totalPagosServicio += 1;

                factura.DetallesFactura.Add(new DetalleFactura()
                {
                    ServicioId = servicio.ServicioId,
                    Monto = servicio.Costo,
                    Descuento = servicio.Descuento,
                    FacturaId = factura.FacturaId,
                    MesId = int.Parse(datos[1])
                }
                ); ;
                total += (servicio.Costo - servicio.Descuento);
                descuento += servicio.Descuento; 
            }


            factura.Total = total;
            factura.Descuento = descuento;

            // Si hay mas de dos servicios aplicar descuento del 10%
            if (servicios.Length > 1)
            {
                decimal desc = (factura.Total * 10 / 100);
                factura.Total -= desc;
                factura.Descuento += desc;
            }

            // Si el servicio que esta pagando tiene mas de 6 pagos no cobrarle un mes
            foreach (var detalle in factura.Servicios)
            {
                if (factura.DetallesFactura.FindAll(x => x.ServicioId == detalle.ServicioId).Count >= 6)
                {
                    factura.Total -= factura.DetallesFactura[5].Monto - factura.DetallesFactura[5].Descuento;
                    factura.Descuento += factura.DetallesFactura[5].Monto - factura.DetallesFactura[5].Descuento;
                    factura.DetallesFactura[5].Monto = 0.0M;
                    factura.DetallesFactura[5].Descuento = 0.0M;
                }
            }

            if (this.servicio.CrearFactura(factura).FacturaId > 0)
            {
                mensaje = "registrado exitosamente.";
                ocurrioError = false;
            }

            return RedirectToAction("Index", new { mensaje = mensaje, ocurrioError = ocurrioError });
        }

    }
}
