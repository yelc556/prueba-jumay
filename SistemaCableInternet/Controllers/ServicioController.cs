﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApplicationSistemaCableInternet.Interface;
using WebApplicationSistemaCableInternet.Models;

namespace WebApplicationSistemaCableInternet.Controllers
{
    public class ServicioController : Controller
    {
        private readonly IServiceServicio servicio;
        private readonly IServicioCliente servicioClientes;
        private readonly IServicioPlanServicio servicioPlanes;
        public ServicioController(IServiceServicio servicio, 
            IServicioCliente servicioClientes, IServicioPlanServicio servicioPlanes)
        {
            this.servicio = servicio;
            this.servicioClientes = servicioClientes;
            this.servicioPlanes = servicioPlanes;
        }
        public IActionResult Index(string mensaje = "", bool ocurrioError = false)
        {
            if (!string.IsNullOrEmpty(mensaje))
            {
                this.ViewData["Clase"] = "success";
                this.ViewData["Mensaje"] = mensaje;

                if (ocurrioError)
                {
                    this.ViewData["Clase"] = "danger";
                }
            }

            List<Servicio> lista = this.servicio.ConsultarListado();
            return this.View(lista);
        }

        public IActionResult Crear()
        {
            var model = new Servicio();
            model.PlanesServicios = this.servicioPlanes.ConsultarListadoPlanServicio();
            model.Clientes = this.servicioClientes.ConsultarClientes();

            return this.View(model);
        }

        [HttpPost]
        public IActionResult Crear(Servicio servicio)
        {
            string mensaje = "Ocurrio un error al intentar guardar el registro, intente mas tarde.";
            bool ocurrioError = true;

            if (this.servicio.RegistrarServicio(servicio))
            {
                mensaje = "registrado exitosamente.";
                ocurrioError = false;
            }

            return RedirectToAction("Index", new { mensaje = mensaje, ocurrioError = ocurrioError });
        }


        public IActionResult Editar(int id)
        {
            var model = this.servicio.ConsultarServicio(id);

            return View(model);
        }

        [HttpPost]
        public IActionResult Editar(int id, Servicio servicio)
        {
            try
            {
                string mensaje = "Ocurrio un error al intentar actualizar el registro, intente mas tarde.";
                bool ocurrioError = true;

                if (this.servicio.ActualizarServicio(servicio))
                {
                    mensaje = "Actualizado exitosamente.";
                    ocurrioError = false;
                }
                return RedirectToAction("Index", new { mensaje = mensaje, ocurrioError = ocurrioError });
            }
            catch
            {
                return View();
            }
        }

    }
}
