﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using WebApplicationSistemaCableInternet.Models;
using WebApplicationSistemaCableInternet.Utilitarios;

namespace WebApplicationSistemaCableInternet.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly Utilitario utilitario;
        public HomeController(ILogger<HomeController> logger)
        {
            this._logger = logger;
            this.utilitario = new Utilitario();
        }

        public IActionResult Index()
        {
            //DbConexion db = new DbConexion();
            //SqlCommand sqlCommand = new SqlCommand()
            //{
            //    CommandType = CommandType.StoredProcedure,
            //    CommandText = "ObtenerClientes"
            //};

            //List<Cliente> clientes = this.utilitario.ParserListaObjeto<List<Cliente>>(db.Conexion(sqlCommand));

            //return JsonConvert.DeserializeObject<List<Producto>>(responseBody);

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
