﻿namespace WebApplicationSistemaCableInternet.Models
{
    public class PlanServicio
    {
        public int PlanServicioId { get; set; }
        public int TipoServicioId { get; set; }
        public string Servicio { get; set; }
        public string Descripcion { get; set; }
        public decimal Costo { get; set; }
        public decimal Descuento { get; set; }
    }
}
