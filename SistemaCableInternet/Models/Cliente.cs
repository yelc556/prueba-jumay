﻿namespace WebApplicationSistemaCableInternet.Models
{
    public class Cliente
    {
        public int ClienteId { get; set; }
        public string Codigo { get; set; }

        public string Apellido { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
    }
}
