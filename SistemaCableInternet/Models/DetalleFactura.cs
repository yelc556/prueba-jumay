﻿using System.Collections.Generic;

namespace WebApplicationSistemaCableInternet.Models
{
    public class Mes
    {
        public int MesId { get; set; }
        public string Nombre { get; set; }
        public bool IsChecked { get; set; }
    }

    public class DetalleFactura
    {
        public int DetalleFacturaId { get; set; }
        public int FacturaId { get; set; }
        public int ServicioId { get; set; }
        public int MesId { get; set; }
        public decimal Monto { get; set; }
        public decimal Descuento { get; set; }

        public List<Mes> Meses { get; set; }

        // Mostrar datos en el detalle
        public string TipoServicio { get; set; }
        public string Plan { get; set; }
        public string Mes { get; set; }

    }
}
