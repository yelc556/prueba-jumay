﻿using System.Collections.Generic;

namespace WebApplicationSistemaCableInternet.Models
{
    public class Servicio
    {
        public int ServicioId { get; set; }
        public int PlanServicioId { get; set; }
        public int ClienteId { get; set; }

        // Datos Del Plan
        public string TipoServicio { get; set; }

        public string Descripcion { get; set; }
        public decimal Costo { get; set; }
        public decimal Descuento { get; set; }

        // Datos Del Cliente
        public string Apellido { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }

        public List<PlanServicio> PlanesServicios { get; set; }

        public List<Cliente> Clientes { get; set; }

        public List<Mes> Pagos { get; set; }

    }
}
