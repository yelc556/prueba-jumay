﻿using System;
using System.Collections.Generic;

namespace WebApplicationSistemaCableInternet.Models
{
    public class Factura
    {
        public int FacturaId { get; set; }
        public int ClienteId { get; set; }

        public DateTime FechaCreacion { get; set; }
        public string Numero { get; set; }
        public string Serie { get; set; }
        public decimal Total { get; set; }
        public decimal Descuento { get; set; }

        // Campos para mostrar en la vista
        public string Codigo { get; set; }
        public string Apellido { get; set; }
        public string Nombre { get; set; }

        public Cliente Cliente { get; set; }
        public List<Servicio> Servicios { get; set; }
        public List<DetalleFactura> DetallesFactura { get; set; }
    }
}
