﻿using System.Collections.Generic;
using WebApplicationSistemaCableInternet.Models;

namespace WebApplicationSistemaCableInternet.Interface
{
    public interface IServicioCliente
    {
        List<Cliente> ConsultarClientes();
        Cliente ConsultarCliente(int id);
        bool CrearCliente(Cliente cliente);
        bool EliminarCliente(int idCliente);
    }
}
