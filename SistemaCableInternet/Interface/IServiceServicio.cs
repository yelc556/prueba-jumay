﻿using System.Collections.Generic;
using WebApplicationSistemaCableInternet.Models;

namespace WebApplicationSistemaCableInternet.Interface
{
    public interface IServiceServicio
    {
        List<Servicio> ConsultarListado();
        Servicio ConsultarServicio(int id);
        List<Servicio> ConsultarServiciosCliente(int clienteId);
        bool RegistrarServicio(Servicio servicio);
        bool ActualizarServicio(Servicio servicio);
    }
}
