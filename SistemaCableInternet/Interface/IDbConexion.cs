﻿using Microsoft.Data.SqlClient;
using System.Data;

namespace WebApplicationSistemaCableInternet.Interface
{
    public interface IDbConexion
    {
        bool OcurrioError { get; }
        string MensajeError { get; }
        DataTable Conexion(SqlCommand sqlCommand);
    }
}
