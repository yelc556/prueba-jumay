﻿using System.Collections.Generic;
using WebApplicationSistemaCableInternet.Models;

namespace WebApplicationSistemaCableInternet.Interface
{
    public interface IServicioFactura
    {
        List<Factura> ConsultarListado();
        Factura ConsultarFactura(int id);
        List<Mes> ConsultarMesesPago(int clienteId, int servicioId);
        Factura CrearFactura(Factura factura);
    }
}
