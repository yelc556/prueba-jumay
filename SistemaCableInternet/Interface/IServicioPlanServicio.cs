﻿using System.Collections.Generic;
using WebApplicationSistemaCableInternet.Models;

namespace WebApplicationSistemaCableInternet.Interface
{
    public interface IServicioPlanServicio
    {
        List<PlanServicio> ConsultarListadoPlanServicio();
        PlanServicio ConsultarPlanServicio(int id);
        bool ActualizarPlanServicio(PlanServicio planServicio);
    }
}
