﻿using Microsoft.Data.SqlClient;
using System;
using System.Data;
using WebApplicationSistemaCableInternet.Interface;

namespace WebApplicationSistemaCableInternet.Implementacion
{
    public class DbConexion : IDbConexion
    {
        private bool ocurrioError;
        private string mensajeError;

        public DbConexion()
        {
            this.ocurrioError = false;
            this.mensajeError = string.Empty;
        }

        public bool OcurrioError
        {
            get { return this.ocurrioError; }
        }

        public string MensajeError
        {
            get { return this.mensajeError; }
        }

        public DataTable Conexion(SqlCommand sqlCommand)
        {
            string connectionString = "Server = YELC\\SQLEXPRESS01; Initial Catalog = jumay; Persist Security Info = False; User ID = dev; Password = ne56r10u; MultipleActiveResultSets = False; Encrypt = False; TrustServerCertificate = False; Connection Timeout = 30; ";

            DataTable data = new DataTable();
            using SqlConnection connectionDb = new SqlConnection(connectionString);
            try
            {
                connectionDb.Open();
                sqlCommand.Connection = connectionDb;

                data.Load(sqlCommand.ExecuteReader());
            }
            catch (Exception ex)
            {
                this.ocurrioError = true;
                this.mensajeError = ex.Message;
            }
            finally
            {
                connectionDb.Close();
            }

            return data;
        }
    }
}
