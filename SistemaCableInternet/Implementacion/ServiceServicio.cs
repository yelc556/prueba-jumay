﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using WebApplicationSistemaCableInternet.Constantes;
using WebApplicationSistemaCableInternet.Interface;
using WebApplicationSistemaCableInternet.Models;
using WebApplicationSistemaCableInternet.Utilitarios;

namespace WebApplicationSistemaCableInternet.Implementacion
{
    public class ServiceServicio : IServiceServicio
    {
        private readonly IDbConexion db;
        private readonly Utilitario utilitario;

        public ServiceServicio(IDbConexion db)
        {
            this.db = db;
            this.utilitario = new Utilitario();
        }

        List<Servicio> IServiceServicio.ConsultarListado()
        {
            SqlCommand sqlCommand = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = SP.SP_ConsultarServicios
            };

            return this.utilitario.ParserListaObjeto<List<Servicio>>(db.Conexion(sqlCommand));
        }

        List<Servicio> IServiceServicio.ConsultarServiciosCliente(int clienteId)
        {
            SqlCommand sqlCommand = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = SP.SP_ConsultarServicios
            };

            sqlCommand.Parameters.AddWithValue("@ServicioId", 0);
            sqlCommand.Parameters.AddWithValue("@ClienteId", clienteId);

            return this.utilitario.ParserListaObjeto<List<Servicio>>(db.Conexion(sqlCommand));
        }


        Servicio IServiceServicio.ConsultarServicio(int id) { 
            SqlCommand sqlCommand = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = SP.SP_ConsultarServicios
            };

            sqlCommand.Parameters.AddWithValue("@ServicioId", id);

            return this.utilitario.ParserObjeto<Servicio>(db.Conexion(sqlCommand));
        }

        public bool ActualizarServicio(Servicio servicio)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_ActualizarPlanServicio
                };

                sqlCommand.Parameters.AddWithValue("@PlanServicioId", servicio.ClienteId);
                sqlCommand.Parameters.AddWithValue("@Descripcion", servicio.PlanServicioId);
                sqlCommand.Parameters.AddWithValue("@Costo", servicio.Costo);
                sqlCommand.Parameters.AddWithValue("@Descuento", servicio.Descuento);

                this.db.Conexion(sqlCommand);

                return !this.db.OcurrioError;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool RegistrarServicio(Servicio servicio)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_RegistrarServicio
                };

                sqlCommand.Parameters.AddWithValue("@ClienteId", servicio.ClienteId);
                sqlCommand.Parameters.AddWithValue("@PlanServicioId", servicio.PlanServicioId);

                this.db.Conexion(sqlCommand);

                return !this.db.OcurrioError;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
