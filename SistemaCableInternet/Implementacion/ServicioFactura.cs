﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using WebApplicationSistemaCableInternet.Constantes;
using WebApplicationSistemaCableInternet.Interface;
using WebApplicationSistemaCableInternet.Models;
using WebApplicationSistemaCableInternet.Utilitarios;

namespace WebApplicationSistemaCableInternet.Implementacion
{
    public class ServicioFactura : IServicioFactura
    {
        private readonly IDbConexion db;
        private readonly Utilitario utilitario;

        public ServicioFactura(IDbConexion db)
        {
            this.db = db;
            this.utilitario = new Utilitario();
        }

        public Factura ConsultarFactura(int id)
        {
            SqlCommand sqlCommand = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = SP.SP_ConsultarFacturas
            };

            sqlCommand.Parameters.AddWithValue("@FacturaId", id);
            var factura = this.utilitario.ParserObjeto<Factura>(db.Conexion(sqlCommand));

            // Obtener detalles de la factura
            sqlCommand = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = SP.SP_ConsultarDetallesFacturas
            };

            sqlCommand.Parameters.AddWithValue("@FacturaId", id);
            factura.DetallesFactura = 
                this.utilitario.ParserListaObjeto<List<DetalleFactura>>(db.Conexion(sqlCommand));

            return factura;
        }

        public List<Factura> ConsultarListado()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_ConsultarFacturas
                };

                return this.utilitario.ParserListaObjeto<List<Factura>>(db.Conexion(sqlCommand));
            }
            catch (Exception ex)
            {
                return new List<Factura>();
            }
        }

        public List<Mes> ConsultarMesesPago(int clienteId, int servicioId)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_ConsultarMesesPago
                };

                sqlCommand.Parameters.AddWithValue("@ClienteId", clienteId);
                sqlCommand.Parameters.AddWithValue("@ServicioId", servicioId);

                return this.utilitario.ParserListaObjeto<List<Mes>>(db.Conexion(sqlCommand));
            }
            catch (Exception ex)
            {
                return new List<Mes>();
            }
        }


        public Factura CrearFactura(Factura factura)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_CrearFactura
                };

                sqlCommand.Parameters.AddWithValue("@ClienteId", factura.ClienteId);
                sqlCommand.Parameters.AddWithValue("@Numero", factura.Numero);
                sqlCommand.Parameters.AddWithValue("@Serie", factura.Serie);
                sqlCommand.Parameters.AddWithValue("@Total", factura.Total);
                sqlCommand.Parameters.AddWithValue("@Descuento", factura.Descuento);
                
                var detalles = factura.DetallesFactura;
                factura = this.utilitario.ParserObjeto<Factura>(db.Conexion(sqlCommand));

                // Guardar Detalles

                foreach (var detalle in detalles)
                {
                    SqlCommand sqlCommandDF = new SqlCommand()
                    {
                        CommandType = CommandType.StoredProcedure,
                        CommandText = SP.SP_CrearDetalleFactura
                    };

                    sqlCommandDF.Parameters.AddWithValue("@FacturaId", factura.FacturaId);
                    sqlCommandDF.Parameters.AddWithValue("@ServicioId", detalle.ServicioId);
                    sqlCommandDF.Parameters.AddWithValue("@MesId", detalle.MesId);
                    sqlCommandDF.Parameters.AddWithValue("@Monto", detalle.Monto);
                    sqlCommandDF.Parameters.AddWithValue("@Descuento", detalle.Descuento);

                    this.db.Conexion(sqlCommandDF);
                }

                return factura;
            }
            catch (Exception Ex)
            {
                return new Factura();
            }
        }

    }
}
