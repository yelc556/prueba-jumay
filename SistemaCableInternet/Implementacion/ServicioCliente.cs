﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using WebApplicationSistemaCableInternet.Constantes;
using WebApplicationSistemaCableInternet.Interface;
using WebApplicationSistemaCableInternet.Models;
using WebApplicationSistemaCableInternet.Utilitarios;

namespace WebApplicationSistemaCableInternet.Implementacion
{
    public class ServicioCliente : IServicioCliente
    {
        private readonly IDbConexion db;
        private readonly Utilitario utilitario;

        public ServicioCliente(IDbConexion db)
        {
            this.db = db;
            this.utilitario = new Utilitario();
        }

        public List<Cliente> ConsultarClientes()
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.ObtenerClientes
                };

                return this.utilitario.ParserListaObjeto<List<Cliente>>(db.Conexion(sqlCommand));
            }
            catch (Exception ex)
            {
                return new List<Cliente>();
            }
        }

        public Cliente ConsultarCliente(int id)
        {
            SqlCommand sqlCommand = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = SP.ObtenerClientes
            };

            sqlCommand.Parameters.AddWithValue("@ClienteId", id);

            return this.utilitario.ParserObjeto<Cliente>(db.Conexion(sqlCommand));
        }


        public bool CrearCliente(Cliente cliente)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_RegistrarCliente
                };

                sqlCommand.Parameters.AddWithValue("@Codigo", cliente.Codigo);
                sqlCommand.Parameters.AddWithValue("@Nombre", cliente.Nombre);
                sqlCommand.Parameters.AddWithValue("@Apellido", cliente.Apellido);
                sqlCommand.Parameters.AddWithValue("@Direccion", cliente.Direccion);
                sqlCommand.Parameters.AddWithValue("@Telefono", cliente.Telefono);
                sqlCommand.Parameters.AddWithValue("@Correo", cliente.Codigo);

                this.db.Conexion(sqlCommand);

                return !this.db.OcurrioError;
            }
            catch (Exception Ex)
            {
                return false;
            }
        }

        public bool EliminarCliente(int idCliente)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_BorrarCliente
                };

                sqlCommand.Parameters.AddWithValue("@ClienteId", idCliente);

                this.db.Conexion(sqlCommand);

                return !this.db.OcurrioError;
            }
            catch (Exception Ex)
            {
                return false;
            }
        }

    }
}
