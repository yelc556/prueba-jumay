﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using WebApplicationSistemaCableInternet.Constantes;
using WebApplicationSistemaCableInternet.Interface;
using WebApplicationSistemaCableInternet.Models;
using WebApplicationSistemaCableInternet.Utilitarios;

namespace WebApplicationSistemaCableInternet.Implementacion
{
    public class ServicioPlanServicio : IServicioPlanServicio
    {
        private readonly IDbConexion db;
        private readonly Utilitario utilitario;

        public ServicioPlanServicio(IDbConexion db)
        {
            this.db = db;
            this.utilitario = new Utilitario();
        }

        List<PlanServicio> IServicioPlanServicio.ConsultarListadoPlanServicio()
        {
            SqlCommand sqlCommand = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = SP.SP_ConsultarPlanServicio
            };

            return this.utilitario.ParserListaObjeto<List<PlanServicio>>(db.Conexion(sqlCommand));
        }

        PlanServicio IServicioPlanServicio.ConsultarPlanServicio(int id)
        {
            SqlCommand sqlCommand = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = SP.SP_ConsultarPlanServicio
            };

            sqlCommand.Parameters.AddWithValue("@PlanServicioId", id);

            return this.utilitario.ParserObjeto<PlanServicio>(db.Conexion(sqlCommand));
        }

        public bool ActualizarPlanServicio(PlanServicio planServicio)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand()
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SP.SP_ActualizarPlanServicio
                };

                sqlCommand.Parameters.AddWithValue("@PlanServicioId", planServicio.PlanServicioId);
                sqlCommand.Parameters.AddWithValue("@Descripcion", planServicio.Descripcion);
                sqlCommand.Parameters.AddWithValue("@Costo", planServicio.Costo);
                sqlCommand.Parameters.AddWithValue("@Descuento", planServicio.Descuento);

                this.db.Conexion(sqlCommand);

                return !this.db.OcurrioError;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
