﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationSistemaCableInternet.Constantes
{
    public static class SP
    {
        // Clientes
        public const string ObtenerClientes = "SP_ObtenerClientes";
        public const string SP_RegistrarCliente = "SP_RegistrarCliente";
        public const string SP_BorrarCliente = "SP_BorrarCliente";

        // Planes
        public const string SP_ConsultarPlanServicio = "SP_ObtenerPlanServicio";
        public const string SP_ActualizarPlanServicio = "SP_ActualizarPlanServicio";


        // Servicios
        public const string SP_ConsultarServicios = "SP_ObtenerServicios";
        public const string SP_RegistrarServicio = "SP_RegistrarServicio";
        public const string SP_ActualizarServicio = "SP_ActualizarServicio";

        // Facturas
        public const string SP_ConsultarFacturas = "SP_ObtenerFacturas";
        public const string SP_ConsultarDetallesFacturas = "SP_ObtenerDetallesFactura";
        public const string SP_CrearFactura = "SP_RegistrarFactura";
        public const string SP_CrearDetalleFactura = "SP_RegistrarDetalleFactura";
        public const string SP_ConsultarMesesPago = "SP_ObtenerPagosFaltantesAnio";

    }
}
